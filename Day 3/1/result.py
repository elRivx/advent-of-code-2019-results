# Imports / Importaciones

from collections import defaultdict

# Load Data / Cargando Datos (input.txt)

wire1, wire2 = open("input.txt").readlines()
#wire1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
#wire2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"


def parse_instruction_list(wire):
    turns = wire.strip().split(",")
    return [(turn[0], int(turn[1:])) for turn in turns]


def get_coords_for_instructions(initCoords, instructions):
    x, y = initCoords
    direction, count = instructions
    for i in range(count):
        if direction == 'L':
            x = x - 1
        elif direction == 'R':
            x = x + 1
        elif direction == 'U':
            y = y - 1
        elif direction == 'D':
            y = y + 1
        yield(x, y)


def coords_for_wires(wire):
    instructions = parse_instruction_list(wire)
    current_coord = (0, 0)
    for instruction in instructions:
        coords = list(get_coords_for_instructions(current_coord, instruction))
        for coord in coords:
            yield coord
        current_coord = coords[-1]


def place_wire_in_grid(grid, wire):
    local_grid = set()
    for coords in coords_for_wires(wire):
        if coords in local_grid:
            continue
        local_grid.add(coords)
        grid[coords] += 1


grid = defaultdict(int)
place_wire_in_grid(grid, wire1)
place_wire_in_grid(grid, wire2)

intersections = {k for k, v in grid.items() if v > 1}
manhattans = [abs(x) + abs(y) for x, y in intersections]
closets = min(manhattans)

print(closets)
