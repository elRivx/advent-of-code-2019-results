masses = [int(x) for x in open("input.txt").readlines()]
fuels = [int(m / 3) - 2 for m in masses]
totalFuel = sum(fuels)
print(totalFuel)
