import os


def init(noun, verb):

    input_prg = [int(x) for x in open("input.txt").readline().split(",")]
    input_prg[1] = noun
    input_prg[2] = verb
    return {
        "program": input_prg,
        "pc": 0,
    }


def simulate_computer(m):
    opcode = m["program"][m["pc"]]

    if opcode == 99:
        print("Program Ended. P0 = {}".format(m["program"][0]))
        return

    ptr_input1 = m["program"][m["pc"] + 1]
    ptr_input2 = m["program"][m["pc"] + 2]
    ptr_output = m["program"][m["pc"] + 3]

    input1 = m["program"][ptr_input1]
    input2 = m["program"][ptr_input2]

    if opcode == 1:
        output = input1 + input2

    elif opcode == 2:
        output = input1 * input2

    m["program"][ptr_output] = output
    m["pc"] += 4

    simulate_computer(m)


def result(noun, verb):
    computer = init(noun, verb)
    simulate_computer(computer)
    return computer["program"][0]


for noun in range(99):
    for verb in range(99):
        print(noun, verb)
        computer = init(noun, verb)
        simulate_computer(computer)
        output = computer["program"][0]
        print(noun, verb, output, 100 * noun + verb)

        if output == 19690720:
            os._exit(0)
