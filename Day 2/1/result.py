import os
input_prg = [int(x) for x in open("input.txt").readline().split(",")]
input_prg[1] = 12
input_prg[2] = 2
computer = {
    "program": input_prg,
    "pc": 0,
}


def simulate_computer(m):
    opcode = m["program"][m["pc"]]
    print("PC: {}, Opcode {}".format(m["pc"], opcode))

    if opcode == 99:
        print("Program Ended. P0 = {}".format(m["program"][0]))
        return

    ptr_input1 = m["program"][m["pc"] + 1]
    ptr_input2 = m["program"][m["pc"] + 2]
    ptr_output = m["program"][m["pc"] + 3]

    input1 = m["program"][ptr_input1]
    input2 = m["program"][ptr_input2]

    if opcode == 1:
        output = input1 + input2
        print(" > {} + {} = {} (PRT {}, {}, {})".format(input1,
                                                        input2, output, ptr_input1, ptr_input2, ptr_output))
    elif opcode == 2:
        output = input1 * input2
        print(" > {} * {} = {} (PRT {}, {}, {})".format(input1,
                                                        input2, output, ptr_input1, ptr_input2, ptr_output))

    m["program"][ptr_output] = output
    m["pc"] += 4

    simulate_computer(m)


simulate_computer(computer)
