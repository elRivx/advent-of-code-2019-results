# Advent of Code 2019 Results / Resultados Advent of Code 2019

## Contenidos/Contents

- [Información](#Información)
- [Tecnologías](#Tecnologías)
- [Objetivos](#Objetivos)

## Información/Information

En éste repositorio iré subiendo mis respuestas al reto de Advent of Code 2019.

In this repository you will see my Advent of Code 2019 answers.

## Tecnologías/Technologies

Se han usado para su realización:

- Python 3 (Anaconda 3)

I'm only use:

- Python 3 (Anaconda 3)


## Objetivos/Goals

- [x] Día 1 completado/Day one complete https://adventofcode.com/2019/day/1
- [x] Día 2 completado/Day two complete https://adventofcode.com/2019/day/2
- [x] Día 3 completado/Day three complete https://adventofcode.com/2019/day/3
- [x] Día 4 completado/Day four complete https://adventofcode.com/2019/day/4
- [x] Día 5 completado/Day five complete https://adventofcode.com/2019/day/5
- [x] Día 6 completado/Day six complete https://adventofcode.com/2019/day/6
- [x] Día 7 completado/Day seven complete https://adventofcode.com/2019/day/7
- [x] Día 8 completado/Day eight complete https://adventofcode.com/2019/day/8
- [x] Día 9 completado/Day nine complete https://adventofcode.com/2019/day/9
- [x] Día 10 completado/Day ten complete https://adventofcode.com/2019/day/10
- [x] Día 11 completado/Day eleven complete https://adventofcode.com/2019/day/11
- [x] Día 12 completado/Day twelve complete https://adventofcode.com/2019/day/12
- [x] Día 13 completado/Day thirteen complete https://adventofcode.com/2019/day/13
- [x] Día 14 completado/Day fourteen complete https://adventofcode.com/2019/day/14
- [x] Día 15 completado/Day fifteen complete https://adventofcode.com/2019/day/15
- [x] Día 16 completado/Day sixteen complete https://adventofcode.com/2019/day/16
- [ ] se cambiará tirulo cuando termine